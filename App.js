import * as React from 'react';
import { View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import {
  Search,
  ImageCard,
} from './screens';

const Stack = createStackNavigator();

export default function App() { // TODO screen{} via context?
  return (
    <View style={{ flex: 1 }}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="Search" component={Search} />
          <Stack.Screen name="ImageCard" component={ImageCard} />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
}
