// import mock from './mockData.json';
import { KEY, CX } from './config';

class Api {
  // TODO remove
  // getData() {
  //     return Promise.resolve(mock);
  // }
  getData(value) {
    return fetch(`https://www.googleapis.com/customsearch/v1?key=${KEY}&cx=${CX}&q=${value}&searchType=image`)
      .then((data) => data.json());
  }
}

export default new Api();
