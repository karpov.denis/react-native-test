import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  Linking,
} from 'react-native';
// import { string, shape, func } from 'prop-types';
import { Ionicons } from '@expo/vector-icons';

import styles from './styles';
import { cutLink } from './utils';

function ImageCard(props) {
  const {
    route: { params },
    navigation: { navigate },
  } = props;

  return (
    <View style={styles.wrapper}>
      <Ionicons
        style={styles.backBtn}
        name="md-arrow-back"
        size={42}
        color="#7D3C98"
        onPress={() => navigate('Search')}
      />
      <ScrollView
        contentContainerStyle={styles.imageWrapper}
        minimumZoomScale={1}
        maximumZoomScale={5}
        ZoomScale={1}
      >
        <ImageBackground source={{ uri: params.link }} style={styles.image} />
      </ScrollView>
      <View style={styles.textWrapper}>
        <Text style={styles.title}>{params.title}</Text>
        <Text style={styles.text}>{params.snippet}</Text>
        <TouchableOpacity style={styles.buttonWrapper}>
          <Text
            title={cutLink(params.displayLink)}
            onPress={() => Linking.openURL(params.image.contextLink)}
            style={styles.button}
          >
            {cutLink(params.displayLink)}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default ImageCard;
