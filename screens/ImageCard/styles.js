import { StyleSheet } from 'react-native';

import { height } from '../../constants/Layout';


const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'row',
    height: '100%',
    backgroundColor: '#000',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: height * 0.3,
  },
  imageWrapper: {
    height: '100%',
    flex: 1,
    justifyContent: 'center',
  },
  textWrapper: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    padding: 10,
    minHeight: height * 0.20,
    backgroundColor: '#BEB9B8',
    borderWidth: 1,
    opacity: 0.8,
  },
  title: {
    fontSize: 20,
    marginBottom: 10,
  },
  text: {
    fontSize: 16,
    color: '#000',
  },
  buttonWrapper: {
    marginTop: 'auto',
  },
  button: {
    marginLeft: 'auto',
    marginBottom: 5,
    marginRight: 5,
    padding: 10,
    paddingRight: 25,
    paddingLeft: 25,
    backgroundColor: '#000',
    color: '#fff',
  },
  backBtn: {
    position: 'absolute',
    top: 50,
    left: 10,
    zIndex: 10,
  },
});

export default styles;
