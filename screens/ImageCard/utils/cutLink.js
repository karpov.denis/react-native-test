const cutLink = link => link.replace('www.', '');

export default cutLink;
