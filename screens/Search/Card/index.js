import React, { useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';

import styles from './style';

const Card = (props) => {
  const {
    item: { link, title },
    navigation: { navigate },
    item,
  } = props;

  const [loading, setLoading] = useState(null);

  return (
    <View style={styles.wrapper}>
      <TouchableOpacity onPress={() => navigate('ImageCard', item)}>
        <ImageBackground
          style={styles.image}
          source={{ uri: link }}
          onLoadStart={() => setLoading(true)}
          onLoadEnd={() => setLoading(false)}
        >
          {loading && (
          <View style={styles.spinner}>
            <ActivityIndicator size="large" color="orange" />
          </View>
          )}
          <View style={styles.textWrapper}>
            <Text
              style={styles.text}
              numberOfLines={1}
            >
              {title}
            </Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  );
};

export default Card;
