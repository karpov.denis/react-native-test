import { StyleSheet } from 'react-native';

import { width } from '../../../constants/Layout';

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    width: '50%',
    marginBottom: 30,
  },
  image: {
    width: width * 0.35,
    height: 100,
    backgroundColor: 'lightgrey',
  },
  textWrapper: {
    marginTop: 'auto',
    backgroundColor: '#BEB9B8',
    borderWidth: 1,
    opacity: 0.8,
    padding: 5,
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#000',
  },
  spinner: {
    paddingTop: 25,
    flex: 1,
    alignItems: 'center',
  },
});

export default styles;
