import React, { useState, useEffect } from 'react';
import { Ionicons } from '@expo/vector-icons';
import {
  Text,
  View,
  TextInput,
  SafeAreaView,
  FlatList,
  Platform,
} from 'react-native';

import api from '../../api';
import Card from './Card';
import styles from './styles';

const Search = ({ navigation }) => {
  const [hasError, setErrors] = useState(false);
  const [data, setData] = useState([]);
  const [query, setQuery] = useState();

  const inputRef = React.createRef();

  const onSubmitEditing = (e) => {
    if (Platform.OS === 'web') {
      setQuery(e.target.value);
    }

    setQuery(e.nativeEvent.text);
  };
  const onPress = () => {
    setQuery('');
    setData([]);
    inputRef.current.clear();
  };

  const getData = (value) => {
    if (!value) {
      return;
    }

    const response = api.getData(value);
    response
      .then((res) => setData(res.items))
      .catch((err) => setErrors(err));
  };

  useEffect(() => {
    getData(query);
  }, [query]);

  return (
    <SafeAreaView style={styles.SafeViewAndroid}>
      <View style={styles.front}>
        <Text style={styles.header}> Image search </Text>
        <View style={styles.searchWrap}>

          <TextInput
            style={styles.input}
            placeholder="Search"
            onSubmitEditing={onSubmitEditing}
            ref={inputRef}
            clearButtonMode="always"
            clearTextOnFocus
          />

          <Ionicons
            style={styles.cleanIcon}
            name="md-close-circle"
            size={32}
            color="darkgrey"
            onPress={onPress}
          />
        </View>

        <FlatList
          style={styles.list}
          data={data}
          renderItem={({ item }) => (
            <Card
              item={item}
              navigation={navigation}
            />
          )}
          keyExtractor={(item) => item.title}
          numColumns={2}
        />

      </View>
    </SafeAreaView>
  );
};

export default Search;
