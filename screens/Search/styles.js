/* eslint-disable react-native/no-color-literals */
import {
  StyleSheet,
  Platform,
  StatusBar,
} from 'react-native';

const styles = StyleSheet.create({
  SafeViewAndroid: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  front: {
    backgroundColor: '#faebd7',
    flex: 1,
  },
  searchWrap: {
    backgroundColor: '#faebd7',
    position: 'relative',
  },
  header: {
    fontSize: 35,
    textAlign: 'center',
  },
  input: {
    margin: 3,
    padding: 8,
    paddingRight: 35,
    fontSize: 18,
    backgroundColor: 'whitesmoke',
  },
  cleanIcon: {
    position: 'absolute',
    right: 5,
    top: 2,
  },
  list: {
    paddingTop: 20,
  },
});

export default styles;
